vcpkg_from_github(
    OUT_SOURCE_PATH SOURCE_PATH
    REPO            danvratil/qcoro
    REF             e66e8882acdea951cd379045167fc9be30564942
    SHA512          722799bde1857b6cb489d7e2f79719b04a9e5133d7ce302931edbda19c308f7e49aa20a7375bd50510c01d8414d59d3bb893b30d650a74516c4666a0399e091a
    HEAD_REF        main
    PATCHES
        0001-Fix-deprecation-warning-for-QWebSocket-error-in-6.5.patch
)

vcpkg_configure_cmake(
    SOURCE_PATH ${SOURCE_PATH}
    OPTIONS 
        -DBUILD_TESTING=OFF
        -DQCORO_DISABLE_DEPRECATED_TASK_H=ON
)
vcpkg_install_cmake()

vcpkg_fixup_cmake_targets(
    CONFIG_PATH lib/cmake/QCoro6
    TARGET_PATH share/QCoro6
    DO_NOT_DELETE_PARENT_CONFIG_PATH
)
vcpkg_fixup_cmake_targets(
    CONFIG_PATH lib/cmake/QCoro6Core
    TARGET_PATH share/QCoro6Core
    DO_NOT_DELETE_PARENT_CONFIG_PATH
)
vcpkg_fixup_cmake_targets(
    CONFIG_PATH lib/cmake/QCoro6Coro
    TARGET_PATH share/QCoro6Coro
    DO_NOT_DELETE_PARENT_CONFIG_PATH
)
if(VCPKG_TARGET_IS_LINUX)
    vcpkg_fixup_cmake_targets(
        CONFIG_PATH lib/cmake/QCoro6DBus
        TARGET_PATH share/QCoro6DBus
        DO_NOT_DELETE_PARENT_CONFIG_PATH
    )
endif()
vcpkg_fixup_cmake_targets(
    CONFIG_PATH lib/cmake/QCoro6Network
    TARGET_PATH share/QCoro6Network
    DO_NOT_DELETE_PARENT_CONFIG_PATH
)
vcpkg_fixup_cmake_targets(
    CONFIG_PATH lib/cmake/QCoro6Qml
    TARGET_PATH share/QCoro6Qml
    DO_NOT_DELETE_PARENT_CONFIG_PATH
)
vcpkg_fixup_cmake_targets(
    CONFIG_PATH lib/cmake/QCoro6Quick
    TARGET_PATH share/QCoro6Quick
    DO_NOT_DELETE_PARENT_CONFIG_PATH
)
vcpkg_fixup_cmake_targets(
    CONFIG_PATH lib/cmake/QCoro6WebSockets
    TARGET_PATH share/QCoro6WebSockets
)

file(INSTALL "${SOURCE_PATH}/LICENSE" DESTINATION "${CURRENT_PACKAGES_DIR}/share/${PORT}" RENAME copyright)
file(INSTALL "${CMAKE_CURRENT_LIST_DIR}/usage" DESTINATION "${CURRENT_PACKAGES_DIR}/share/${PORT}")

file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/include")
file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/share")
