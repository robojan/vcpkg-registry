vcpkg_fail_port_install(ON_TARGET "uwp")
vcpkg_check_linkage(ONLY_STATIC_LIBRARY)

vcpkg_from_github(
    OUT_SOURCE_PATH SOURCE_PATH
    REPO            andreasbuhr/cppcoro
    REF             7cc9433436fe8f2482138019cfaafce8e1d7a896
    SHA512          38477408158c325c3143ad1c22459dbb5f74923170e3fc70c17d9f4a96a387dad9ef74c28ac1155389d695e5310858c465101856e0af993736d0fb602959e578
    HEAD_REF        master
    PATCHES         add_a_missing_noexcept_marker.patch
)

vcpkg_configure_cmake(
    SOURCE_PATH ${SOURCE_PATH}
    PREFER_NINJA
    OPTIONS
        -DBUILD_TESTING=False
)
vcpkg_install_cmake()

vcpkg_fixup_cmake_targets(
    CONFIG_PATH lib/cmake/${PORT}
)

file(INSTALL     ${SOURCE_PATH}/LICENSE.txt
     DESTINATION ${CURRENT_PACKAGES_DIR}/share/${PORT}
     RENAME      copyright
)
vcpkg_copy_pdbs()

file(REMOVE_RECURSE ${CURRENT_PACKAGES_DIR}/debug/include)
file(REMOVE_RECURSE ${CURRENT_PACKAGES_DIR}/debug/share)
