
set(qt_version ${VERSION})

if(VCPKG_TARGET_IS_WINDOWS)
    set(target_platform "win64_msvc2019_64")
    set(target_install_platform "msvc2019_64")
    set(target_osname "windows")
    set(target_target "desktop")
elseif(VCPKG_TARGET_IS_MINGW)
    set(target_platform "win64_mingw")
    set(target_install_platform "mingw_64")
    set(target_osname "windows")
    set(target_target "desktop")
elseif(VCPKG_TARGET_IS_LINUX)
    set(target_platform "gcc_64")
    set(target_install_platform "gcc_64")
    set(target_osname "linux")
    set(target_target "desktop")
elseif(VCPKG_TARGET_IS_OSX)
    set(target_platform "clang_64")
    set(target_install_platform "macos")
    set(target_osname "mac")
    set(target_target "desktop")
else()
    message(FATAL "Unsupported target platform: ${TARGET_TRIPLET}")
endif()

if(VCPKG_HOST_IS_WINDOWS)
    vcpkg_download_distfile(installer_exe
        URLS https://github.com/miurahr/aqtinstall/releases/download/v3.1.2/aqt.exe
        FILENAME aqt.exe
        SHA512 6d8f224b26b3e5f1c512d8f7bfbbb5e8b8a3855210ff2b793d84e31afce937bc56982467e69d0d0e5b7ed65e4ffe5e511251409e06191d4786767270ff78f8f7
        )
elseif(VCPKG_HOST_IS_LINUX)
    message(WARNING "Semi-supported platform. Please install aqt with pip install aqtinstall and 7z")
    set(installer_exe "aqt")
elseif(VCPKG_HOST_IS_OSX)
    message(WARNING "Semi-supported platform. Please install aqt with pip install aqtinstall and 7z")
    set(installer_exe "aqt")
else()
    message(FATAL "Unsupported host platform: ${HOST_TRIPLET}")
endif()


vcpkg_execute_required_process(
    COMMAND ${installer_exe} install-qt ${target_osname} ${target_target} ${qt_version} ${target_platform} -m all
    WORKING_DIRECTORY ${CURRENT_BUILDTREES_DIR}
    LOGNAME build-${TARGET_TRIPLET}-rel
)

file(GLOB installed_files ${CURRENT_BUILDTREES_DIR}/${qt_version}/${target_install_platform}/* )
foreach(file ${installed_files})
    message("Copying ${file} to ${CURRENT_PACKAGES_DIR}")
    file(COPY ${file} DESTINATION ${CURRENT_PACKAGES_DIR})
endforeach()
# file(GLOB debug_dll ${CURRENT_BUILDTREES_DIR}/${VERSION}/${platform}/bin/*d.dll)
# foreach(file ${debug_dll})
#     message("Copying ${file} to ${CURRENT_PACKAGES_DIR}/bin/debug")
#     file(COPY ${file} DESTINATION ${CURRENT_PACKAGES_DIR}/bin/debug)
# endforeach()
# file(GLOB debug_dll ${CURRENT_BUILDTREES_DIR}/${VERSION}/${platform}/lib/*d.lib ${CURRENT_BUILDTREES_DIR}/${VERSION}/${platform}/lib/*d.prl)
# foreach(file ${debug_dll})
#     message("Copying ${file} to ${CURRENT_PACKAGES_DIR}/lib/debug")
#     file(COPY ${file} DESTINATION ${CURRENT_PACKAGES_DIR}/lib/debug)
# endforeach()

# vcpkg_install_copyright()
