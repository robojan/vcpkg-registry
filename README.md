robojan-vcpkg-registry

To add this registry to vcpkg create the vcpkg-configuration.json file in the vcpkg root.

Add the following entry
```
        {
            "kind": "git",
            "repository": "https://gitlab.com/robojan/vcpkg-registry.git"
        }
```

### Sequence of commands for adding a new package
```
PS C:\toolchains\robojan-vcpkg-registry> git add .\ports\
PS C:\toolchains\robojan-vcpkg-registry> git commit -m "[cppcoro] Fixing portfile.cmake"
[main 350bad8] [cppcoro] Fixing portfile.cmake
 1 file changed, 2 insertions(+), 2 deletions(-)
PS C:\toolchains\robojan-vcpkg-registry> git rev-parse HEAD:ports/cppcoro
86351c1879ab0b9d88b44104b2dc49c2e7d69ec8
PS C:\toolchains\robojan-vcpkg-registry> git add .\versions\
PS C:\toolchains\robojan-vcpkg-registry> git commit --amend --no-edit
```